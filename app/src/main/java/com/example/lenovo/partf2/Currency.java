package com.example.lenovo.partf2;

import android.os.Parcel;
import android.os.Parcelable;

public class Currency {
    public String ask;
    public String bid;

    public Currency(String ask, String bid) {
        this.ask = ask;
        this.bid = bid;
    }
}
