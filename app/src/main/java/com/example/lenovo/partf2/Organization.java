package com.example.lenovo.partf2;

import android.os.Parcel;
import android.os.Parcelable;

import org.w3c.dom.Entity;

import java.security.KeyStore;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Organization implements Parcelable {

    public String id;
    public Integer orgType;
    public String title;
    public String phone;
    public String address;
    public String link;
    public Map<String, Currency> currencies;

    public String getId() {
        return id;
    }

    public Integer getOrgType() {
        return orgType;
    }

    public String getAddress() {
        return address;
    }

    public String getTitle() {
        return title;
    }

    public String getPhone() {
        return phone;
    }

    public String getLink() {
        return link;
    }

    public static Parcelable.Creator<Organization> getCREATOR() {
        return CREATOR;
    }

    protected Organization(Parcel in) {
        id = in.readString();
        orgType = in.readInt();
        title = in.readString();
        phone = in.readString();
        address = in.readString();
        link = in.readString();
        currencies = new HashMap<>();
        for (int i = 0; i < 3; i++) {
            String name = in.readString();
            String ask = in.readString();
            String bid = in.readString();
            if (!(name == null || ask == null || bid == null)) {
                currencies.put(name, new Currency(ask, bid));
            }
        }
    }

    public static final Parcelable.Creator<Organization> CREATOR = new Parcelable.Creator<Organization>() {
        @Override
        public Organization createFromParcel(Parcel in) {
            return new Organization(in);
        }

        @Override
        public Organization[] newArray(int size) {
            return new Organization[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeInt(orgType);
        dest.writeString(title);
        dest.writeString(phone);
        dest.writeString(address);
        dest.writeString(link);
        Iterator<Map.Entry<String, Currency>> entries = currencies.entrySet().iterator();
        int i = 0;
        while (entries.hasNext() && i < 3) {
            Map.Entry<String, Currency> entry = entries.next();
            dest.writeString(entry.getKey());
            dest.writeString(entry.getValue().ask);
            dest.writeString(entry.getValue().bid);
            i++;
        }
    }
}
