package com.example.lenovo.partf2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import java.io.BufferedReader;
import java.util.Iterator;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OrganizationActivity extends AppCompatActivity {

    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.phone)
    TextView phone;
    @BindView(R.id.address)
    TextView address;
    @BindView(R.id.currencies)
    TextView currencies;

    Organization organization;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_organization);
        ButterKnife.bind(this);
        if (getIntent() != null && getIntent().getParcelableExtra(MainActivity.KEY_LABEL) != null) {
            organization = getIntent().getParcelableExtra(MainActivity.KEY_LABEL);
            title.setText(organization.getTitle());
            phone.setText(organization.getPhone());
            address.setText(organization.getAddress());
            currencies.setText("");
            Iterator<Map.Entry<String, Currency>> entries = organization.currencies.entrySet().iterator();
            int i = 0;
            while (entries.hasNext() && i < 3) {
                Map.Entry<String, Currency> entry = entries.next();
                currencies.setText(currencies.getText() + "\n" + entry.getKey()
                        + " ask " + entry.getValue().ask
                        + " bid " + entry.getValue().bid);
                i++;
            }
        }
    }
}

