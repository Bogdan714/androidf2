package com.example.lenovo.partf2;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ListAdapter extends BaseAdapter {

    private Context context;
    private List<Organization> organizations;

    public ListAdapter(Context context) {
        this.context = context;
        this.organizations = new ArrayList<>();
    }

    public void addAll(List<Organization> OrganizationList) {
        organizations.addAll(OrganizationList);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return organizations.size();
    }

    @Override
    public Organization getItem(int position) {
        return organizations.get(position);
    }

    @Override
    public long getItemId(int position) {
        return organizations.get(position).hashCode();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;
        ViewHolder holder;
        if (rowView == null) {
            rowView = LayoutInflater.from(context)
                    .inflate(R.layout.list_item, parent, false);
            holder = new ViewHolder(rowView);
            rowView.setTag(holder);
        } else {
            holder = (ViewHolder) rowView.getTag();
        }

        if (getItem(position).getTitle().length() > 30) {
            holder.title.setText(getItem(position).getTitle().substring(0, 28) + "...");
        } else {
            holder.title.setText(getItem(position).getTitle());
        }
        holder.phone.setText(getItem(position).getPhone());
        return rowView;
    }

    static class ViewHolder {

        @BindView(R.id.bank_name)
        TextView title;
        @BindView(R.id.phone)
        TextView phone;

        public ViewHolder(View root) {
            ButterKnife.bind(this, root);
        }
    }

}
