package com.example.lenovo.partf2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.OnItemClick;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MainActivity extends AppCompatActivity {
    public static final String KEY_LABEL = "key";
    ListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        adapter = new ListAdapter(this);
        final ListView listView = findViewById(R.id.list);
        Retrofit.getData(new Callback<BankSystem>() {

            @Override
            public void success(BankSystem bankSystem, Response response) {
                Toast.makeText(MainActivity.this, "ok, " + bankSystem.date, Toast.LENGTH_SHORT).show();
                adapter.addAll(bankSystem.organizations);
                listView.setAdapter(adapter);
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(MainActivity.this, "failure", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @OnItemClick(R.id.list)
    public void onItemClick(int position) {
        Intent intent = new Intent(this, OrganizationActivity.class);
        intent.putExtra(KEY_LABEL, adapter.getItem(position));
        startActivity(intent);
    }

}
